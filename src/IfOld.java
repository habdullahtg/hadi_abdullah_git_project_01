import java.util.Scanner;

public class IfOld {
    public static void main(String[] args) {

        System.out.println("Please enter your age: ");
        Scanner input = new Scanner(System.in);

        int age = input.nextInt();

        if (age > 25 ) System.out.println("You are mature");
        else System.out.println("You are immature");
    }
}
